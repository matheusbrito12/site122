
---
title: "About"
date: 2021-09-18T16:59:34-03:00
draft: false
---
>
>>>>>>>>>>>>>>>>># Sobre
>>>>>>>>>>![foto](/img/dark1.1.jpg)
## Dark Souls focuses on dungeon exploring and the tension and fear that arise when players encounter enemies in this setting. The game is a spiritual successor to Demon's Souls, which in turn is considered a spiritual successor to the King's Field games. The game takes place in an open world environment and uses a third-person perspective. The player battles using various weapons and strategies to survive in a dark fantasy world. Online features allow players to share the play experience without need for direct communication.

## With respect to Demon's Souls, there are more weapons, spells, and new classes. Before release, it was also said to be "significantly harder", with more enemies and more difficult encounters. It includes leaving messages to other players, seeing others' deaths, and player versus player (as well as a new co-op system); similar to the online functions in Demon's Souls. The items the player uses in these games are significantly different.




